//
//  FirstVC.swift
//  BirthNote
//
//  Created by Admin on 25/03/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import RealmSwift

class BirthNote:Object{
   @objc dynamic var name:String = ""
   @objc dynamic var birthdate:String = ""
   
    
   
    
   
    
}





class FirstVC: UITableViewController {
    

    var notes = [BirthNote]()

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
//    func makeUserWithname(_name:String,birthdate:String,birthcount:Birthdate?)->BirthNote{
//        let newUser = BirthNote()
//        newUser.name = name
//    }
    
    
    @IBAction func addNote(_ sender: UIBarButtonItem) {
        
        var nameTextFiled:UITextField?
        var birthTextField:UITextField?
        
        let alert = UIAlertController(title: "BirthNoteTaker", message: "", preferredStyle: .alert)
        
        alert.addTextField { (nameTF) in
            nameTextFiled = nameTF
            nameTF.placeholder = "Name"
        }
        
        alert.addTextField { (birthdateTF) in
            birthTextField = birthdateTF
            birthdateTF.placeholder = "mm/dd/yyyy"
        }
        
        let action = UIAlertAction(title: "Add", style: .default) { (add) in
           let name = nameTextFiled?.text
           let birthdate = birthTextField?.text
            
            let takenotes = 
           self.notes.append(takenotes)
            
           self.tableView.reloadData()
            
        }
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let takenotes = notes[indexPath.row]
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        cell.textLabel?.text = takenotes.name
        cell.detailTextLabel?.text = takenotes.birthdate
        
        return cell
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "DetailVC", sender: nil)
    }
    
    


}

