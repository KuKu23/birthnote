//
//  SecondVC.swift
//  BirthNote
//
//  Created by Admin on 19/03/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

//class BirthCountDown{
//    var countdown:String
//    
//    init(countdown:String) {
//        self.countdown = countdown
//    }
//}


class SecondVC: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
       
    }
    
    @IBAction func countDown(_ sender: UIButton) {
       let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
        

    }
    
    @objc func countdown(){
        let birthdate = datePicker.date
        let diffDateComponents = Calendar.current.dateComponents([.day,.hour,.minute], from: Date(), to: birthdate)
        textLabel.text = "\(diffDateComponents.day!) days:\(diffDateComponents.hour!) hours:\(diffDateComponents.minute!) minutes"
        
       
    }
    
    
   

    @IBAction func backBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

}

